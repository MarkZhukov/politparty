<#ftl encoding='UTF-8'>
<#--<head>
    <head>
        <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    </head>
</head>
<body>
<#if error??>
<div class="alert alert-danger" role="alert">${error}</div>
</#if>
<div class="content-block">
    <form class="form-horizontal" action="/signUp" method="post">
        <input name="login" placeholder="Логин">
        <input name="password" placeholder="Пароль">
        <input type="submit">
    </form>
</div>
</body>
-->



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign up</title>
    <link href="../../css/style.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
<#if error??>
<div class="alert alert-danger" role="alert">${error}</div>
</#if>
<div class="navigation">
    <ul>
        <li><a href="/plane">[about]</a></li>
        <li><a href="/login">Sign in</a></li>
        <li><a href="/signUp" class="sign_up">Sign Up</a></li>
    </ul>
</div>

<div class="sign_up">
    <form class="form-signup" role="form" enctype="multipart/form-data" action="/signUp" method="post">
        <h2 class="form-signup-heading">Sign up</h2>
        <input type="text" class="form-control" placeholder="name" name="name" required autofocus>
        <input type="text"  class="form-control" placeholder="username" name="login" required autofocus>
        <input type="password" class="form-control" placeholder="password" name="password1" required autofocus>
        <input type="password" class="form-control" placeholder="confirm password" name="password2" required autofocus>
        <input type="text" class="form-control" placeholder="head quarter city" name="city" autofocus>
        <input type="text" class="form-control" placeholder="email" name="email" autofocus>
        <p>affix photo</p>
        <#--<input type="file" class="form-control" name="photo">-->
        <button class="btn btn-lg btn-primary btn-block" type="submit">sign up</button>
    </form>
</div>

<#--<#include "footer.ftl">-->
</body>
</html>