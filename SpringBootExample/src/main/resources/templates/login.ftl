<#ftl encoding='UTF-8'>
<#--<head>
    <head>
        <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    </head>
</head>
<body>
<#if model.error.isPresent()>
<div class="alert alert-danger" role="alert">Логин или пароль введены неверно</div>
</#if>
ПРИВЕТ!
<div class="content-block">
    <form class="form-horizontal" action="/login" method="post">
        <input name="login" placeholder="Логин">
        <input name="password" placeholder="Пароль">
        <input type="submit">
    </form>
</div>
</body>
-->


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign in</title>
    <link href="../../css/style.css" rel="stylesheet">
    <#--<link href="../../css/bootstrap.min.css" rel="stylesheet"/>-->

</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/plane">[about]</a></li>
        <li><a href="/login" class="sign_in">Sign in</a></li>
        <li><a href="/signUp">Sign up</a></li>
    </ul>
</div>
<#if model.error.isPresent()>
<div class="alert alert-danger" role="alert">Логин или пароль введены неверно</div>
</#if>
<div class="container">
    <form class="form-signin" role="form" method="post" action="/login">
        <h2>Sign in</h2>
        <input type="text" class="form-control" name="login" placeholder="username" required autofocus>
        <input type="password" class="form-control" name="password" placeholder="password" required autofocus>
        <label class="checkbox">
            <input type="checkbox" value="remember-me" name="remember"> Remember me
        </label>
        <br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">sign in</button>
    </form>
</div>

<#--<#include "footer.ftl">-->
</body>
</html>