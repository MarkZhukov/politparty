<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Profile</title>
    <link href="../../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/user/home">News</a></li>
        <li><a href="/plane">Plane</a></li>
        <li><a href="/user/hq">Head Quarters</a></li>
        <li><a href="/user/profile/${model.user.login}" class="profile">Profile</a></li>
    <#--<#if isLogIn == 1>-->
        <li><a href="/logout">Out</a></li>
    <#--<#else >-->
        <#--<li><a href="/login">Sign in</a></li>-->
    <#--</#if>-->
    </ul>
</div>

<div class="profile">
<#if model.photo?has_content>
    <img src="${model.user.photo.path}" alt="..." class="img-rounded" width="auto" height="300">
<#else >
    <img src="../../images/0.jpg" alt="..." class="img-rounded" width="auto" height="300">
</#if>
    <blockquote>
        <h4>${model.user.login}</h4>
    <#if model.user.name?has_content>
        <p>${model.user.name}</p>
    </#if>
    <#if model.user.headQuarter?has_content>
        <p>${model.user.headQuarter.city}</p>
    </#if>
        <p>${model.user.role}</p>

    <#--<#if person.description?has_content>-->
        <#--<p>${person.description}</p>-->
    <#--</#if>-->
    <#--<#if person.instLink?has_content>-->
        <#--<a href="${person.instLink}">Instagram</a>-->
    <#--</#if>-->
    <#--<#if person.twitLink?has_content>-->
        <#--<a href="${person.twitLink}">Twitter</a>-->
    <#--</#if>-->
    <#--<#if person.faceLink?has_content>-->
        <#--<a href="${person.faceLink}">Facebook</a>-->
    <#--</#if>-->
    <#--<#if person.vkLink?has_content>-->
        <#--<a href="${person.kLink}">VK</a>-->
    <#--</#if>-->
    </blockquote>
<#--<form action="/edit_person/${person.id}" method="get">
    <button class="comment" type="submit">Edit</button>
</form>-->
</div>
<#--<#include "footer.ftl">-->
</body>
</html>