

CREATE TABLE public.news_post
(
  id bigint NOT NULL DEFAULT nextval('news_post_id_seq'::regclass),
  description character varying(255),
  head_line character varying(255),
  photo character varying(255),
  photo_id integer NOT NULL,
  CONSTRAINT news_post_pkey PRIMARY KEY (id),
  CONSTRAINT fk1urnew8lg2qnhug0sfo7hayaj FOREIGN KEY (photo_id)
  REFERENCES public.photo (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.news_post
  OWNER TO postgres;


INSERT INTO news_post(id,head_line,description,photo_id)
VALUES (1,'yeah','first post',1)