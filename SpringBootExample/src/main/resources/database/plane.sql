
CREATE TABLE public.plane
(
  id bigint NOT NULL DEFAULT nextval('plane_id_seq'::regclass),
  text character varying(255),
  photo_id bigint,
  CONSTRAINT plane_pkey PRIMARY KEY (id),
  CONSTRAINT fknsg9vph5wg7ucjdakkyuqfuod FOREIGN KEY (photo_id)
  REFERENCES public.photo (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.plane
  OWNER TO postgres;

INSERT into plane(id,text) VALUES (34,'здесь могла быть ваша реклама, но сейчас расположен плант работы партии зла на ближайший год или день, всего вам плохо, администрация');

INSERT into plane(id,text) VALUES (44,'реклама так и не появиласьт, но мы будем стараться, искать другое зло, чтобы в этом мире страдал и не знал горя, без уважения, всем привет');