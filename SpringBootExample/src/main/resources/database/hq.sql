

CREATE TABLE public.head_quarter
(
  id bigint NOT NULL DEFAULT nextval('head_quarter_id_seq'::regclass),
  address character varying(255),
  city character varying(255),
  CONSTRAINT head_quarter_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.head_quarter
  OWNER TO postgres;



INSERT INTO head_quarter(id,address,city)
VALUES (1,'Vespoochy beach, 35','Los-Angelеs');

INSERT INTO head_quarter(id,address,city)
VALUES (2,'8 street, 9','New-York');

INSERT INTO head_quarter(id,address,city)
VALUES (999,'8 street, 9n','New-York');