package ru.itis.forms;

import lombok.*;
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateHqForm {

    private String city;
    private String address;

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }
}
