package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.repositories.HQRepository;
import ru.itis.repositories.UsersRepository;
import ru.itis.security.role.Role;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private HQRepository hqRepository;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public void register(UserRegistrationForm userForm) {
        // создаем нового пользователя для БД с ролью USER
        User newUser = User.builder()
                .login(userForm.getLogin())
                .hashPassword(passwordEncoder.encode(userForm.getPassword1()))
                .name(userForm.getName())
                .email(userForm.getEmail())
                .headQuarter(hqRepository.findByCity(userForm.getCity()))
                .role(Role.USER)
                .build();
        // сохраняем пользователя
        usersRepository.save(newUser);
    }
}
