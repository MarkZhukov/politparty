package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.CreateHqForm;
import ru.itis.models.HeadQuarter;
import ru.itis.repositories.HQRepository;

import java.util.List;

@Service
public class HQServiceImpl implements HQService {

    @Autowired
    private HQRepository hqRepository;

    @Override
    public HeadQuarter findByCity(String city) {
        return hqRepository.findByCity(city);
    }



    @Override
    public HeadQuarter findById(Long id) {
        return hqRepository.findById(id);
    }

    @Override
    public List<HeadQuarter> findAllByCity(String city) {
        return hqRepository.findAllByCity(city);
    }

    @Override
    public List<HeadQuarter> findAll() {
        return hqRepository.findAll();
    }

    @Override
    public boolean add(CreateHqForm form) {
        HeadQuarter hq = HeadQuarter.builder()
                .city(form.getCity())
                .address(form.getAddress())
                .build();
        hqRepository.save(hq);
        return true;
    }

    @Override
    public boolean delete(Long id) {
        hqRepository.delete(id);
        return true;
    }
}
