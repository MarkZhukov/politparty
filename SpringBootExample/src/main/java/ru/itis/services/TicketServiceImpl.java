package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.State;
import ru.itis.models.Ticket;
import ru.itis.repositories.TicketRepository;
import ru.itis.repositories.UsersRepository;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<Ticket> getTickets() {
        return ticketRepository.findAllByStateNot(State.CLOSED);
    }

    @Override
    public void addNew(Long userId) {
        Ticket ticket = Ticket.builder()
                .state(State.NEW)
                .user(usersRepository.findOne(userId))
                .build();
        ticketRepository.save(ticket);
    }

    @Override
    public void dell(Long userId) {
        ticketRepository.delete(ticketRepository.findByUser_Id(userId));
    }

    @Override
    public void changeToInProgress(Ticket ticket) {
        ticketRepository.delete(ticket);
        ticket.setState(State.IN_PROGRESS);
        ticketRepository.save(ticket);
    }

    @Override
    public void changToClosed(Long userId) {
        Ticket ticket = ticketRepository.findByUser_Id(userId);
        dell(userId);
        ticket.setState(State.CLOSED);
        ticketRepository.save(ticket);
    }

    @Override
    public Ticket getOne(Long userId) {
        return ticketRepository.findByUser_Id(userId);
    }


}
