package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.itis.models.User;
import ru.itis.repositories.TicketRepository;
import ru.itis.repositories.UsersRepository;
import ru.itis.security.details.UserDetailsImpl;
import ru.itis.security.role.Role;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public User getUserByAuthentication(Authentication authentication) {
        UserDetailsImpl currentUserDetails = (UserDetailsImpl)authentication.getPrincipal();
        User currentUserModel = currentUserDetails.getUser();
        Long currentUserId = currentUserModel.getId();
        return usersRepository.findOne(currentUserId);
    }

    @Override
    public void makeSuperUser(Long id) {
        //usersRepository.updateRole(Role.SUPERUSER,id);
        User user = usersRepository.findOne(id);
        user.setRole(Role.SUPERUSER);
        usersRepository.save(user);
    }

    @Override
    public void kill(Long id) {
        ticketRepository.delete(ticketRepository.findByUser_Id(id));
        usersRepository.delete(id);
    }
}
