package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.NewsPost;
import ru.itis.repositories.NewsPostRepository;
import ru.itis.repositories.PhotoRepository;

import java.util.List;
@Service
public class NewsPostServiceImpl implements NewsPostService {

    @Autowired
    private NewsPostRepository repository;
    @Autowired
    private PhotoRepository photoRepository;

    @Override
    public List<NewsPost> getNews() {
        return repository.findAll();
    }

    @Override
    public List<NewsPost> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean add(String title, String description, String filePath) {
        NewsPost newsPost = NewsPost.builder()
                .headLine(title)
                .description(description)
                .photo(photoRepository.findByPath(filePath))
                .build();
        repository.save(newsPost);
        return true;
    }

    @Override
    public boolean delete(Long id) {
        repository.delete(id);
        return true;
    }
}
