package ru.itis.services;

import ru.itis.forms.CommentForm;
import ru.itis.models.Comment;
import ru.itis.models.HeadQuarter;

import java.util.List;

public interface CommentService {
    List<Comment> findAllByHq(HeadQuarter hq);
    List<Comment> findAllByDescription(String search);
    Comment findById(Long id);
    List<Comment> findAllByHq_Id(Long hq_id);
    void add(String newComment,Long hq_id,Long user_id);
    void delete(Long comment_id);

}
