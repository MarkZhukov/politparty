package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Comment;
import ru.itis.models.HeadQuarter;
import ru.itis.repositories.CommentRepository;
import ru.itis.repositories.HQRepository;
import ru.itis.repositories.UsersRepository;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private HQRepository hqRepository;

    @Autowired
    private UsersRepository usersRepository;



    @Override
    public List<Comment> findAllByHq(HeadQuarter hq) {
        return commentRepository.findAllByHq(hq);
    }

    @Override
    public List<Comment> findAllByDescription(String search) {
        return commentRepository.findAllByDescriptionContaining(search);
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id);
    }

    @Override
    public List<Comment> findAllByHq_Id(Long hq_id) {
        return commentRepository.findAllByHq_Id(hq_id);
    }

    @Override
    public void add(String newComment, Long hq_id, Long user_id) {
        Comment comment = Comment.builder()
                .description(newComment)
                .user(usersRepository.findOne(user_id))
                .hq(hqRepository.findOne(hq_id))
                .build();
        commentRepository.save(comment);
    }

    @Override
    public void delete(Long comment_id) {
        commentRepository.delete(comment_id);
    }
}
