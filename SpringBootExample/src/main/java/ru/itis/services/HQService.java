package ru.itis.services;

import ru.itis.forms.CreateHqForm;
import ru.itis.models.HeadQuarter;

import java.util.List;

public interface HQService {
    HeadQuarter findByCity(String city);
    HeadQuarter findById(Long id);
    List<HeadQuarter> findAllByCity(String city);
    List<HeadQuarter> findAll();
    boolean add(CreateHqForm createHqForm);

    boolean delete(Long id);
}
