package ru.itis.models;

import lombok.*;
import ru.itis.security.role.Role;
import ru.itis.security.states.State;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "owner")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(unique = true)
    private String login;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "head_quarter_id")
    private HeadQuarter headQuarter;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id")
    private Photo photo;

    private String hashPassword;


    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private State state;

    private String hashTempPassword;

    private String email;




}
