package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.itis.services.AdminService;
import ru.itis.services.AuthenticationService;
import ru.itis.services.TicketService;


@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService service;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private TicketService ticketService;

    @GetMapping("/kill/{id}")
    public String kill(@PathVariable("id") String id) {
        authenticationService.kill(Long.parseLong(id));
        return "redirect:/admin/users";
    }

    @GetMapping("/make_super/{id}")
    public String allowSuper(@PathVariable("id") String id) {
        ticketService.changToClosed(Long.parseLong(id));
        authenticationService.makeSuperUser(Long.parseLong(id));
        return "redirect:/user/home";
    }

    @GetMapping("/dis/{id}")
    public String dis(@PathVariable("id") String id) {
        ticketService.changToClosed(Long.parseLong(id));
        return "redirect:/user/home";
    }

    @GetMapping("/users")
    public String getMainAdminPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("users", service.getAllUsers());
        return "admin";
    }

    // позволяет получить временный пароль
    // для того, чтобы зайти под пользователем каким-либо
    @GetMapping("/password/temp/{user-id}")
    public String getNewPasswordOfUserPage(@ModelAttribute("model") ModelMap model,
                                           @PathVariable("user-id") Long userId) {
        // генерируем пароль и отправляем на почту
        service.createTempPassword(userId);
        // скидываем админу страничку - что пароль отправлен на почту
        return "temp_password_page";
    }
}
