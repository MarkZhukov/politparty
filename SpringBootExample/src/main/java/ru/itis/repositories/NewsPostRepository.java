package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.NewsPost;

import java.util.List;

public interface NewsPostRepository extends JpaRepository<NewsPost, Long> {
    List<NewsPost> findAllBy();
    //Optional<NewsPost> findOneByLogin(String login);
    //List<NewsPost> findAllByRole(Role role);
    List<NewsPost> findById(Long newsId);
    List<NewsPost> findByDescription(String description);
}
