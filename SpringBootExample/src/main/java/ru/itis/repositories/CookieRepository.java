package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Cookie;

import java.util.List;

public interface CookieRepository extends JpaRepository<Cookie,Long> {

}
