package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.HeadQuarter;
import ru.itis.models.State;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByHeadQuarter(HeadQuarter head_quarter);
    Optional<User> findOneByLogin(String login);
    List<User> findAllByRoleOrRole(Role a,Role b);
    List<User> findAllByRole(Role role);

    Optional<User> findById(Long userId);


    List<User> findAllByRoleNot(Role role);
}