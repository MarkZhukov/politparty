package ru.itis.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.HeadQuarter;

import java.util.List;
import java.util.Optional;

public interface HQRepository extends JpaRepository<HeadQuarter, Long> {
    HeadQuarter findById(Long id);
    List<HeadQuarter> findAllByCity(String city);
    HeadQuarter findByCity(String city);

}
