<#ftl encoding='UTF-8'>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>News</title>
    <link href="../../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <#--<link rel="stylesheet" type="text/css" href="<@spring.url '../css/style.css'/>"/>-->

</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/plane" class="sign_in">[about]</a></li>
        <li><a href="/login">Sign in</a></li>
        <li><a href="/signUp">Sign up</a></li>

    </ul>
</div>
<div class="news">
    <#list model.news as news>
    <div class="card mb-3">
        <#--<img class="card-img-top" src="../../images/${news.photo.path}" alt="">-->
        <div class="card-body">
            <#--<h4 class="card-title">${news.headLine}</h4>-->
            <p class="card-text">${news.text}</p>
            <#--<p class="card-text"><small class="text-muted">${new.dt}</small></p>-->
            <#--<#if is_admin == 1>-->
            <#--<div class="delete">-->
                <#--<a href="/delete/news/${news.id}"><img src="<@spring.url '/images/delete.png'/>" width="25" height="25"></a>-->
            <#--</div>-->
            <#--</#if>-->
        </div>
    </div>
    </#list>

    <#--<#include "footer.ftl">-->
</body>
</html>










<#--<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello ${name}!</title>
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<h2 class="hello-title">Hello man ${name}!</h2>
<script src="/js/main.js"></script>
</body>
</html>-->