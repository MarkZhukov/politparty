<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Create topic</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/user/home">News</a></li>
        <li><a href="/plane">Plane</a></li>
        <li><a href="/user/hq" class="forum">Forum</a></li>
        <li><a href="/profile/${model.user.login}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    </ul>
</div>

<div class="create_topic">
    <form class="form-createtopic" role="form" method="get" action="/superuser/save_hq">
        <h2>Creating a quarter</h2>
        <input type="text" name="city" class="form-control" placeholder="City" required autofocus>
        <input type="text" name="address" class="form-control" placeholder="Address" required autofocus>
        <#--<p>input like that</p>-->
        <#--<p>#tag #dance #input</p>-->
        <#--<input type="text" name="tags" class="form-control" placeholder="Search tags" required autofocus>-->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Create head quarter</button>
    </form>
</div>

<#--<#include "footer.ftl">-->
</body>
</html>