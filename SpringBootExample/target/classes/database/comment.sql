

CREATE TABLE public.comment
(
  id bigint NOT NULL DEFAULT nextval('comment_id_seq'::regclass),
  description character varying(255),
  head_quarter_id bigint,
  user_id bigint,
  CONSTRAINT comment_pkey PRIMARY KEY (id),
  CONSTRAINT fkfp8k2r7f33wvbbahwvdd3kb0k FOREIGN KEY (user_id)
  REFERENCES public.owner (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkt7iojwaevcbmegrcyv9qd8oo5 FOREIGN KEY (head_quarter_id)
  REFERENCES public.head_quarter (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.comment
  OWNER TO postgres;



INSERT INTO comment(id,description,head_quarter_id,user_id)
VALUES (1,'yeah',1,2);

INSERT INTO comment(id,description,head_quarter_id,user_id)
VALUES (2,'la',1,3);