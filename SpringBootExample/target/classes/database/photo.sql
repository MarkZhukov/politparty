
CREATE TABLE public.owner
(
  id bigint NOT NULL DEFAULT nextval('owner_id_seq'::regclass),
  email character varying(255),
  hash_password character varying(255),
  hash_temp_password character varying(255),
  login character varying(255),
  name character varying(255),
  role character varying(255),
  state character varying(255),
  owner_id bigint,
  head_quarter_id bigint,
  head_quarter bigint,
  photo_id bigint,
  CONSTRAINT owner_pkey PRIMARY KEY (id),
  CONSTRAINT fke6xri6sfnvnay4y3rkoluivva FOREIGN KEY (photo_id)
  REFERENCES public.photo (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fki19v1u7am0cxlv9fc2agf4ho4 FOREIGN KEY (head_quarter_id)
  REFERENCES public.head_quarter (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkoymyiyv3a2x3p5m69xtsp0p32 FOREIGN KEY (owner_id)
  REFERENCES public.head_quarter (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkpxmiraubc49wk5ef5b39jjmhk FOREIGN KEY (head_quarter)
  REFERENCES public.head_quarter (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_1bapitwt0uil8ndhpprv57la7 UNIQUE (login)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.owner
  OWNER TO postgres;



INSERT INTO photo(id,path)
VALUES (1,'0.jpg')