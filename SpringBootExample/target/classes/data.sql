INSERT INTO owner(id, email, name, login, hash_password, role, state, head_quarter_id)
  SELECT 1, 'hahlucker@gmail.com', 'Администратор', 'admin', '$2a$10$CR29hxLcDnapMFQRykDIMemIiZSWidn6DHfomvb3/lV5QWAmRPqa6', 'ADMIN', 'CONFIRMED',1
  WHERE
    NOT EXISTS(
        SELECT id
        FROM owner
        WHERE id = 1
    );